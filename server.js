// modules =================================================
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var exphbs = require('express-handlebars');
var path = require("path");

// configuration ===========================================
// set our port
var port = process.env.PORT || 3000;

// parse application/json 
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override'));

//view engine setup
app.engine('.hbs', exphbs({ extname: '.hbs', defaultLayout: 'index', layoutsDir: __dirname + '/public/' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', '.hbs');

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public'));

// routes ==================================================
require(__dirname + '/app/routes/routes')(app); // configure our local routes
require(__dirname + '/app/api/api')(app); // configure our api routes

// start app ===============================================
// startup our app at http://localhost:3000
app.listen(port);

// shoutout to the user                     
console.log('Magic happens on port ' + port);

// expose app           
exports = module.exports = app;
var Blog = require(__dirname + '/../models/blog.model');
var BlogCategory = require(__dirname + '/../models/blogCategory.model');

module.exports = function (app) {
    app.use(function (req, res, next) {
        // do logging
        console.log('Something is happening route.');
        next(); // make sure we go to the next routes and don't stop here
    });

    app.get('/', function (req, res) {
        res.render(__dirname + '/../../public/views/home', { title: 'Slintel – Sales Intel To Accelerate Growth', data: 'home' });
    });

    app.get('/product', function (req, res) {
        res.render(__dirname + '/../../public/views/product', { title: 'Slintel – Sales Intel To Accelerate Growth', data: 'product' });
    });

    app.get('/blog', function (req, res) {
        Blog.getAllBlogs(function (err, rows) {
            if (err) throw err;
            res.render(__dirname + '/../../public/views/blog', { title: 'Slintel – Sales Intel To Accelerate Growth', data: 'blog', data: rows });
        });
    });

    app.get('/resources', function (req, res) {
        res.render(__dirname + '/../../public/views/resources', { title: 'Slintel – Sales Intel To Accelerate Growth', data: 'resources' });
    });

    app.get('/team', function (req, res) {
        res.render(__dirname + '/../../public/views/team', { title: 'Slintel – Sales Intel To Accelerate Growth', data: 'team' });
    });

    app.get('/careers', function (req, res) {
        res.render(__dirname + '/../../public/views/careers', { title: 'Slintel – Sales Intel To Accelerate Growth', data: 'careers' });
    });

    app.get('/contact', function (req, res) {
        res.render(__dirname + '/../../public/views/contact', { title: 'Slintel – Sales Intel To Accelerate Growth', data: 'contact' });
    });

    // app.get('*', function (req, res) {
    //     res.render(__dirname + '/../../public/views/404', { title: 'Slintel – Sales Intel To Accelerate Growth', data: '404' });
    // });


};
var Blog = require(__dirname + '/../models/blog.model');
var BlogCategory = require(__dirname + '/../models/blogCategory.model');

module.exports = function (app) {
    app.use(function (req, res, next) {
        // do logging
        console.log('Something is happening api.');
        next(); // make sure we go to the next routes and don't stop here
    });

    // blog category
    app.get('/api/siteadmin/blog-category', function (req, res) {
        BlogCategory.getAllBlogCategories(function (err, rows) {
            if (err) throw err;
            res.json(rows);
        });
    });

    // blog
    app.post('/api/siteadmin/blog/create', function (req, res) {
        var blog = {
            title: req.body.title,
            preview_img_url: req.body.previewImgUrl,
            preview_content: req.body.previewContent,
            banner_img_url: req.body.bannerImgUrl,
            author: req.body.author,
            author_img_url: req.body.authorImgUrl,
            article: req.body.article,
            created_date: req.body.createdDate,
            blog_category: req.body.blogCategory
        };
        Blog.createBlog(blog, function (err, row) {
            if (err) throw err;
            res.send('blog created successfully');
        })
    });



};
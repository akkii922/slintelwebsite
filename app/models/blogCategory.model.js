var db = require(__dirname + '/../../config/db.js');

var BlogCategory = {
    getAllBlogCategories: function (callback) {
        return db.query("SELECT * FROM blog_categories", callback);
    }
};

module.exports = BlogCategory;

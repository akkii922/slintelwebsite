var db = require(__dirname + '/../../config/db.js');

var Blog = {
    getAllBlogs: function (callback) {
        return db.query("SELECT * FROM blogs", callback);
    },
    createBlog: function (blog, callback) {
        var sql = "INSERT INTO blogs SET ?";
        return db.query(sql, blog, callback);
    }
};

module.exports = Blog;
